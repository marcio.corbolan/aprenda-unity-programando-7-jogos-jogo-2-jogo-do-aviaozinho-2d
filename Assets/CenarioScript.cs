﻿using UnityEngine;
using System.Collections;

public class CenarioScript : MonoBehaviour {
	float telaLargura;

	// Use this for initialization
	void Start() {
		SpriteRenderer grafico = GetComponent<SpriteRenderer>();

		float imagemAltura = grafico.sprite.bounds.size.y;
		float imagemLargura = grafico.sprite.bounds.size.x;

		float telaAltura = Camera.main.orthographicSize * 2f;
		telaLargura = telaAltura / Screen.height * Screen.width;

		Vector2 novaEscala = transform.localScale;
		novaEscala.x = (telaLargura / imagemLargura) + 0.25f;
		novaEscala.y = telaAltura / imagemAltura;
		this.transform.localScale = novaEscala;

		if (this.name == "CenarioA2") {
			transform.position = new Vector2(telaLargura, 0f);
		}

		GetComponent<Rigidbody2D>().velocity = new Vector2(-3, 0);
	}
	
	// Update is called once per frame
	void Update() {
		if (transform.position.x <= -telaLargura) {
			transform.position = new Vector2(telaLargura, 0f);
		}
	}
}