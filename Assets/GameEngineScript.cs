﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameEngineScript : MonoBehaviour
{
	int score;
	bool start;

	public GameObject inimigoGo;
	public GameObject jogadorGo;
	public Text displayT;
	public float force;
	public Vector2 velocity;

	void Awake ()
	{
		displayT.transform.position = new Vector2 (Screen.width / 2, Screen.height - 200);
		displayT.text = "Toque para iniciar";
		displayT.fontSize = 35;
	}

	// Use this for initialization
	void Start ()
	{
		force = -4;
		score = 0;
		start = false;
	}

	// Update is called once per frame
	void Update ()
	{
		if (start == false) {
			if (Input.GetButtonDown ("Fire1")) {
				start = true;

				displayT.transform.position = new Vector2 ((Screen.width / 5), (Screen.height - 40));
				displayT.fontSize = 50;
				displayT.text = score.ToString ();

				Invoke ("CriarJogador", 0);
				InvokeRepeating ("CriarInimigo", 0.0f, 1.5f);
			}
		}
	}

	public void End ()
	{
		CancelInvoke ("CriarJogador");
		CancelInvoke ("CriarInimigo");
		Invoke ("RecarregaJogo", 0.5f);
	}

	public void MarcaPonto ()
	{
		score++;
		displayT.text = score.ToString ();

		force = force + (-1);
	}

	void CriarJogador ()
	{
		GameObject novoJogador = Instantiate (jogadorGo);
		velocity = new Vector2 (-0.05f, 0.0f);
		novoJogador.transform.position = velocity;
	}

	void CriarInimigo ()
	{
		float telaAltura = Screen.height;
		float inimigoAltura = 10.0f * inimigoGo.transform.localScale.y;
		float telaAlturaMax = telaAltura - (telaAltura - inimigoAltura);
		float telaAlturaMin = -telaAlturaMax;
		float altura = Random.Range (telaAlturaMin, telaAlturaMax);
		GameObject novoInimigo = Instantiate (inimigoGo);
		novoInimigo.transform.position = new Vector2 (15.0f, altura);
	}

	void RecarregaJogo ()
	{
		SceneManager.LoadScene ("Cena01");
	}
}