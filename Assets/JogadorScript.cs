﻿using UnityEngine;
using System.Collections;

public class JogadorScript : MonoBehaviour
{
	GameEngineScript gameEngineS;
	public GameObject penaParticulaGo;

	void Awake ()
	{
		//gameEngineS = Component.GetComponentInParent<GameEngineScript>();
		//gameEngineS = transform.parent.gameObject.GetComponent<GameEngineScript>();
		gameEngineS = GameObject.FindGameObjectWithTag ("GameEngine").GetComponent<GameEngineScript> ();
	}

	// Use this for initialization
	void Start ()
	{
		GetComponent<Rigidbody2D> ().isKinematic = false;
	}

	// Update is called once per frame
	void Update ()
	{
		if (Input.GetButtonDown ("Fire1")) {
			GetComponent<Rigidbody2D> ().velocity = new Vector2 (0, 0);
			GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0, 350f));

			Vector2 jogadorPosicao = this.transform.position + new Vector3 (0, 0, 0);

			GameObject peninhas = Instantiate (penaParticulaGo);
			peninhas.transform.position = jogadorPosicao;
		} else {
			this.transform.rotation = Quaternion.Euler (0, 0, GetComponent<Rigidbody2D> ().velocity.y * 3);
		}

		float jogadorAlturaPixel = Camera.main.WorldToScreenPoint (transform.position).y;

		if ((jogadorAlturaPixel > Screen.height) || (jogadorAlturaPixel < 0)) {
			EfeitoColisao ();
			gameEngineS.End ();
		}
	}

	void FindClosestObject ()
	{
		/*
		GameObject[] gos;
		gos = GameObject.Find("Inimigo");
		GameObject closest = null;
		float distance = Mathf.Infinity;
		Vector3 position = transform.position;

		foreach (GameObject go in gos) {
			Vector3 diff = go.transform.position - position;
			float curDistance = diff.sqrMagnitude;
			if (curDistance < distance) {
				closest = go;
				distance = curDistance;
			}
		}

		Debug.DrawLine (this.transform.position, closest.transform.position);
		*/
	}

	void OnCollisionEnter2D ()
	{
		EfeitoColisao ();
		gameEngineS.End ();
	}

	void EfeitoColisao ()
	{
		GetComponent<Collider2D> ().enabled = false;
		GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
		GetComponent<Rigidbody2D> ().AddForce (new Vector2 (-300f, 350f));
		GetComponent<Rigidbody2D> ().AddTorque (300f);
		GetComponent<SpriteRenderer> ().color = new Color (1.0f, 0.35f, 0.35f);
	}
}