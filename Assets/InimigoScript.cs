﻿using UnityEngine;
using System.Collections;

public class InimigoScript : MonoBehaviour
{
	GameEngineScript gameEngineS;
	GameObject jogadorGo;
	bool scored = false;

	void Awake ()
	{
		//gameEngineS = Component.GetComponentInParent<GameEngineScript>();
		//gameEngineS = transform.parent.gameObject.GetComponent<GameEngineScript>();
		gameEngineS = GameObject.FindGameObjectWithTag ("GameEngine").GetComponent<GameEngineScript> ();
		jogadorGo = gameEngineS.jogadorGo;
	}

	// Use this for initialization
	void Start ()
	{
		GetComponent<Rigidbody2D> ().velocity = new Vector2 (gameEngineS.force, 0);
	}

	// Update is called once per frame
	void Update ()
	{
		if (GetComponent<Rigidbody2D> ().transform.position.x < -25) {
			Destroy (this.gameObject);
		} else {
			if (GetComponent<Rigidbody2D> ().transform.position.x < jogadorGo.transform.position.x) {
				if (!scored) {
					scored = true;
					EfeitoUltrapasar ();
					gameEngineS.MarcaPonto ();
				}
			}
		}
	}

	void EfeitoUltrapasar ()
	{
		GetComponent<Rigidbody2D> ().isKinematic = false;
		GetComponent<Rigidbody2D> ().velocity = new Vector2 (gameEngineS.force, 0f);
		GetComponent<SpriteRenderer> ().color = new Color (1.0f, 0.35f, 0.35f);
	}
}