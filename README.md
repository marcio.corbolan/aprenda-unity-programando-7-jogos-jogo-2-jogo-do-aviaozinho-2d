# Aprenda Unity Programando 7 Jogos - Jogo 2 - Jogo do aviãozinho 2D

### Progresso

Aguardando início

### Objetivo

Criar o jogo nº2 conforme as [instruções](https://www.udemy.com/curso-completo-unity-3d/learn/lecture/4903172#overview).

### Observações

IDE:  [Unity 5.3.4f1](https://download.unity3d.com/download_unity/fdbb5133b820/UnityDownloadAssistant-5.3.4f1.exe)<br />
Linguagem: [C#](https://dotnet.microsoft.com/)

### Contribuição

Esse projeto foi terminado e aceita contribuições.

## Licença

Curso: [Aprenda Unity Programando 7 Jogos](https://www.udemy.com/curso-completo-unity-3d/)